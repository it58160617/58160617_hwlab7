drop table if exists Product;

create table Product
(
	PROD_ID		int not null,
	PROD_Name	varchar(30) not null,
	PRIMARY KEY(PROD_ID)
)ENGINE=InnoDB;
