drop table if exists Auction;

create table Auction
(
        AUC_ID         int NOT NULL ,
        TimeStamp          datetime NOT NULL,
	PROD_ID		int NOT NULL,
        Bid          FLOAT NOT NULL,
        PRIMARY KEY(AUC_ID),
	FOREIGN KEY(TimeStamp) REFERENCES Auction_log(TimeStamp)
) ENGINE=InnoDB;

