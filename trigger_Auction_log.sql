drop trigger if exists Auction_create;
drop trigger if exists Auction_update;
drop trigger if exists Auction_delete;

DELIMITER $$

        create trigger Auction_create
        after insert on Auction_log
        for each row
begin
        if (NEW.Action = 'create') then
                if (NEW.Bid > 0) then
                insert into Auction(AUC_ID,TimeStamp,PROD_ID,Bid)
                values (NEW.AUC_ID,NEW.TimeStamp,NEW.PROD_ID,NEW.Bid);
                end if;
        end if;

end $$

        create trigger Auction_update
        after insert on Auction_log
        for each row
begin
        if (NEW.Action = 'update') then
                if (NEW.Bid > 0) then
                update Auction
                set TimeStamp = NEW.TimeStamp
                where PROD_ID = NEW.PROD_ID;
                update Auction
                set Bid = NEW.Bid
                where PROD_ID = NEW.PROD_ID;
                update Auction
                set AUC_ID = NEW.AUC_ID
                where PROD_ID = NEW.PROD_ID;
                end if;
        end if;

end $$

        create trigger Auction_delete
        before insert on Auction_log
        for each row
begin
	DECLARE AUCBID FLOAT;
	
        if (NEW.Action = 'delete') then

		select Bid
		into AUCBID
		from Auction
		where PROD_ID = NEW.PROD_ID;

                set NEW.Bid = AUCBID;
		
		delete from Auction where PROD_ID = NEW.PROD_ID;
        end if;

end $$

DELIMITER ;
